package nwsattrs

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestRadarFormat(t *testing.T) {
	b, err := ioutil.ReadFile("sn.last.1")
	if err != nil {
		t.Errorf("%s", err)
	}
	rh, err := GetRadarHeader(b)
	if err != nil {
		t.Errorf("%s", err)
	}
	fmt.Printf("%+v\n", rh)
}

func TestRadarFormatStormReal(t *testing.T) {
	stationsToTest := []string{"kgwx", "klvx", "kpah", "klbb", "khpx"}
	for _, station := range stationsToTest {

		b, err := getURLData(fmt.Sprintf("http://weather.noaa.gov/pub/SL.us008001/DF.of/DC.radar/DS.p37cr/SI.%s/sn.last", station))
		//b, err := ioutil.ReadFile("sn.last.1")
		if err != nil {
			t.Errorf("%s", err)
		}
		rh, err := GetRadarHeader(b)
		if err != nil {
			t.Errorf("%s", err)
			return
		}
		fmt.Println(station)
		fmt.Println("Is old", rh.IsOld())
		fmt.Println("Scan time duration:", rh.ScanDuration())

		fmt.Printf("%+v\n", rh)
		fmt.Println(rh.GetDate())

	}
}
func TestRadarFormatMesoReal(t *testing.T) {
	stationsToTest := []string{"kgwx", "klvx", "kpah", "klbb"}
	for _, station := range stationsToTest {

		b, err := getURLData(fmt.Sprintf("http://weather.noaa.gov/pub/SL.us008001/DF.of/DC.radar/DS.141md/SI.%s/sn.last", station))
		//b, err := ioutil.ReadFile("sn.last.1")
		if err != nil {
			t.Errorf("%s", err)
		}
		rh, err := GetRadarHeader(b)
		if err != nil {
			t.Errorf("%s", err)
		}
		fmt.Println(station)
		fmt.Println("Is old", rh.IsOld())
		fmt.Println("Scan time duration:", rh.ScanDuration())
		fmt.Printf("%+v\n", rh)
		fmt.Println(rh.GetDate())

	}
}
