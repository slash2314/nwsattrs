package nwsattrs

import (
	"fmt"
	"strings"
	"time"
)

type RadarStation struct {
	Id                      string
	LeftEdgeLon, TopEdgeLat float64
	LatLng
	radarTime, previousTime time.Time
	updated                 bool
}

func NewRadarStation(stationId string) (*RadarStation, error) {
	stationCoords, ok := Stations[strings.ToUpper(stationId)]
	if !ok {
		return nil, fmt.Errorf("Station %s does not exist", stationId)
	}
	stationBounds := stationCoords.LatLngBounds
	rs := &RadarStation{
		Id:          stationId,
		LeftEdgeLon: stationBounds.Sw.Lng,
		TopEdgeLat:  stationBounds.Ne.Lat,
		LatLng:      stationCoords.LatLng,
		updated:     false,
	}
	return rs, nil
}
func (rs RadarStation) Degrees() LatLng {
	return rs.LatLng

}

//This will retrieve all of the station's storm cell data
//and return it as a celldata this will implement the
//Cacher interface
func (rs RadarStation) GetLatest() (interface{}, error) {
	stormCellData, err := rs.ParseStormCells()
	if err != nil {
		return nil, err
	}
	mesoCellData, err := rs.ParseMesoCells()
	if err != nil {
		return nil, err
	}
	tvsCellData, err := rs.ParseTvsCells()
	if err != nil {
		return nil, err
	}

	cd := CellData{
		StormCellData: stormCellData,
		MesoCellData:  mesoCellData,
		TvsCellData:   tvsCellData,
	}
	return cd, nil
}
