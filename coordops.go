package nwsattrs

import (
	"log"
	"math"
	"strconv"
)

type LatLng struct {
	Lat, Lng float64
}

type LatLngBounds struct {
	Sw, Ne LatLng
}

type IPoint interface {
	Degrees() LatLng
}

type IPolygon interface {
	//Needs to be a polygon that has the points in order to draw a polygon.
	//For instance, with four points, they would go: Nw Ne Se Sw to draw a rectangle
	GetPolygon() []LatLng
}

func Contains(poly IPolygon, point IPoint) bool {
	polygon := poly.GetPolygon()
	coord := point.Degrees()
	lat := coord.Lat
	lng := coord.Lng

	npol := len(polygon)
	var latPoints, lngPoints []float64
	for index := 0; index < npol; index++ {
		latPoints = append(latPoints, polygon[index].Lat)
		lngPoints = append(lngPoints, polygon[index].Lng)
	}
	c := false
	j := npol - 1
	for i := 0; i < npol; i++ {
		if (((lngPoints[i] <= lng) && (lng < lngPoints[j])) || ((lngPoints[j] <= lng) && (lng < lngPoints[i]))) && (lat < (latPoints[j]-latPoints[i])*(lng-lngPoints[i])/(lngPoints[j]-lngPoints[i])+latPoints[i]) {
			c = !c
		}
		j = i
	}
	return c

}

func (llb *LatLngBounds) GetPolygon() []LatLng {
	return []LatLng{
		{llb.Ne.Lat, llb.Sw.Lng},
		llb.Ne,
		{llb.Sw.Lat, llb.Ne.Lng},
		llb.Sw,
	}
}

func (ll LatLng) Degrees() LatLng {
	return ll
}

func Distance(point, otherPoint IPoint) float64 {
	src := point.Degrees()
	dst := otherPoint.Degrees()
	avgLat := (src.Lat + dst.Lat) / 2
	nmPerDegree := math.Cos(degreeToRadians(avgLat)) * NMPERDEGLATEQUATOR
	latDifference := math.Abs(src.Lat - dst.Lat)
	lngDifference := math.Abs(src.Lng - dst.Lng)
	return math.Sqrt(math.Pow(NMPERDEGLATEQUATOR*latDifference, 2) + math.Pow(nmPerDegree*lngDifference, 2))
}

func (rs *RadarStation) getLatLon(az, ran string) LatLng {
	azFloat, err := strconv.ParseFloat(az, 64)
	ranFloat, err := strconv.ParseFloat(ran, 64)
	if err != nil {
		log.Println("Error parsing float ", err)
	}
	azFloatRad := degreeToRadians(azFloat - 90) //transposes it 90 degrees because the station's az starts straight to the north of the station, whereas the coordinate plane starts to the west.
	y := ranFloat * math.Cos(azFloatRad)        //Since it is transposed 90 degrees, this gets the amount of nautical miles east or west of the station
	x := ranFloat * math.Sin(azFloatRad)        //Since it is transposed 90 degrees, this gets the amount of nautical miles north or south of the station

	nmPerDegree := math.Cos(degreeToRadians(rs.Lat)) * NMPERDEGLATEQUATOR //calculates the amount the nautical miles per degree longitude at a specific latitude
	stormLat := rs.Lat - (x / NMPERDEGLATEQUATOR)
	stormLon := rs.Lng + (y / nmPerDegree)
	return LatLng{stormLat, stormLon}
}

type MovementVector struct {
	p                  IPoint
	fcstSpeed, fcstDir float64
}

type Mover interface {
	Movement() MovementVector
}

func hourMovementPoint(fcstSpeed, fcstDir string, point IPoint) LatLng {
	speed, _ := strconv.ParseFloat(fcstSpeed, 64)
	dir, _ := strconv.ParseFloat(fcstDir, 64)
	if dir < 180 {
		dir += 180
	} else {
		dir -= 180
	}

	dir = degreeToRadians(dir - 180)
	x := speed * math.Cos(dir)
	y := speed * math.Sin(dir)
	latLng := point.Degrees()
	nmPerDegree := math.Cos(degreeToRadians(latLng.Lat)) * NMPERDEGLATEQUATOR
	pointLat := latLng.Lat - (x / NMPERDEGLATEQUATOR)
	pointLon := latLng.Lng - (y / nmPerDegree)
	return LatLng{pointLat, pointLon}
}

func Radians(point IPoint) LatLng {
	d := point.Degrees()
	return LatLng{degreeToRadians(d.Lat), degreeToRadians(d.Lng)}
}

func Bearing(point, otherPoint IPoint) float64 {
	pR := Radians(point)
	opR := Radians(otherPoint)
	lat1, lng1 := pR.Lat, pR.Lng
	lat2, lng2 := opR.Lat, opR.Lng
	dLng := lng2 - lng1
	y := math.Sin(dLng) * math.Cos(lat2)
	x := math.Cos(lat1)*math.Sin(lat2) -
		math.Sin(lat1)*math.Cos(lat2)*math.Cos(dLng)
	bearing := radiansToDegrees(math.Atan2(y, x))
	integerBearing := int(bearing)
	decimalBearing := bearing - float64(integerBearing)
	integerBearing = (integerBearing + 360) % 360
	return float64(integerBearing) + decimalBearing

}

/*
func (self LatLng) Bearing(other LatLng) float64 {
	lat1, lon1 := degreeToRadians(self.Lat), degreeToRadians(self.Lng)
	lat2, lon2 := degreeToRadians(other.Lat), degreeToRadians(other.Lng)
	dLng := lon2 - lon1
	y := math.Sin(dLng) * math.Cos(lat2)
	x := math.Cos(lat1)*math.Sin(lat2) -
		math.Sin(lat1)*math.Cos(lat2)*math.Cos(dLng)
	bearing := radiansToDegrees(math.Atan2(y, x))
	integerBearing := int(bearing)
	decimalBearing := bearing - float64(integerBearing)
	integerBearing = (integerBearing + 360) % 360
	return float64(integerBearing) + decimalBearing
}
*/
func degreeToRadians(deg float64) float64 {
	return deg * math.Pi / 180
}

func radiansToDegrees(rad float64) float64 {
	return rad / math.Pi * 180
}
