package nwsattrs

import (
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	WATCHREGEX      = "<strong>.*watch/ww(.*)\\.html"
	WATCHNUMREGEXP  = "WWP(.*)"
	LATLONREGEXP    = "[0-9]{8}"
	WATCHTYPEREGEXP = "\n\n(.*WATCH)"
	EXPIREREGEXP    = "- ([0-9]{6})Z"
)

var (
	watchRegExp     = regexp.MustCompile(WATCHREGEX)
	watchNumRegExp  = regexp.MustCompile(WATCHNUMREGEXP)
	latLonRegExp    = regexp.MustCompile(LATLONREGEXP)
	watchTypeRegExp = regexp.MustCompile(WATCHTYPEREGEXP)
	expireRegExp    = regexp.MustCompile(EXPIREREGEXP)
)

type Watch struct {
	Type        string
	Description string
	Polygon     []LatLng
	ExpireTime  string
}

type WatchCache struct{}

func (w WatchCache) GetLatest() (interface{}, error) {
	return GetNWSWatches(), nil
}

func (self Watch) GetPolygon() []LatLng {
	return self.Polygon
}

func parseLatLng(latLngs []string) []LatLng {
	var coordinates []LatLng
	for _, latLng := range latLngs {
		lat, err := strconv.ParseFloat(latLng[:2]+"."+latLng[2:4], 64)
		lon, err := strconv.ParseFloat(latLng[4:6]+"."+latLng[6:], 64)
		if err != nil {
			log.Println(err)
			continue
		}
		if lon < 66 {
			lon += 100
		}
		coordinates = append(coordinates, LatLng{lat, -lon})
	}
	return coordinates
}

func getWatchList() [][]string {
	html, err := getHtml("http://www.spc.noaa.gov/products/watch/")
	if err != nil {
		log.Println(err)
		return [][]string{}
	}
	return watchRegExp.FindAllStringSubmatch(html, -1)
}

func getWatchDescription(watchNumber string) string {
	watchDescriptionUrl := "http://www.spc.noaa.gov/products/watch/wwp" + watchNumber + ".txt"
	watchDescription, err := getHtml(watchDescriptionUrl)
	if err != nil {
		log.Println(err)
		return ""
	}
	return watchDescription
}

func GetNWSWatches() []Watch {
	rawWatchData := getWatchList()
	watchBoxes := make([]Watch, 0)
	for _, watch := range rawWatchData {
		watchDescription := "<br>" + getWatchDescription(watch[1])
		watchNumberResult := watchNumRegExp.FindStringSubmatch(watchDescription)
		if len(watchNumberResult) == 0 {
			log.Println("Something was wrong with the watch number error result:", watchDescription)
			continue
		}
		number, err := strconv.Atoi(watchNumberResult[1])
		if err != nil {
			log.Println(err)
			continue
		}
		watchLatLonUrl := "http://www.srh.noaa.gov/data/WNS/SAW" + strconv.Itoa(number)
		coordinatesInfo, err := getHtml(watchLatLonUrl)
		if err != nil {
			log.Println(err)
			continue
		}

		//In the case where the warning is still listed, but it is canceled.
		if strings.Contains(coordinatesInfo, "CANCELLED") {
			continue
		}

		currentTime := time.Now().UTC()
		coordinatesInfoResults := expireRegExp.FindStringSubmatch(coordinatesInfo)
		if len(coordinatesInfoResults) == 0 {
			log.Println("There was a problem with the watch", coordinatesInfo)
			continue
		}
		expireTimeZulu := coordinatesInfoResults[1]
		expireTimeParsed, err := time.Parse("200601021504", currentTime.Format("200601")+expireTimeZulu)
		if err != nil {
			log.Println("Error parsing watch expire time.", expireTimeZulu)
		}
		if currentTime.After(expireTimeParsed) {
			continue
		}
		watchType := watchTypeRegExp.FindStringSubmatch(watchDescription)[1]
		latLonPairs := latLonRegExp.FindAllString(coordinatesInfo, -1)
		latLons := parseLatLng(latLonPairs)
		watchDescription = strings.Replace(watchDescription, "\n", "<br>", -1)
		watchBox := Watch{
			Description: watchDescription,
			Polygon:     latLons,
			Type:        watchType,
			ExpireTime:  expireTimeParsed.Format(time.RFC1123Z),
		}

		watchBoxes = append(watchBoxes, watchBox)
	}
	return watchBoxes
}
