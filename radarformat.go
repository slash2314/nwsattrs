package nwsattrs

import (
	"bytes"
	"encoding/binary"
	"time"
)

type RadarHeaderBlock struct {
	MessageCode     int16
	Days            int16 //Number of days since 1 Jan 1970
	MidnightHours   int32 // Number of seconds since midnight
	LengthOfMessage int32
	SourceID        int16
	DestinationID   int16
	NumberOfBlocks  int16
	ProductDescriptionBlock
}

func (rh *RadarHeaderBlock) GetDate() time.Time {
	//Minus 1 to include the epoc day
	radarDay := time.Unix(0, 0).UTC().AddDate(0, 0, int(rh.Days)-1)
	return radarDay.Add(time.Second * time.Duration(rh.MidnightHours))
}

/*
	Source wikipedia https://en.wikipedia.org/wiki/NEXRAD
	about Volume Coverage Patterns (VCP)
	11: Has the best overall volume converage
	211: Improves range-obscured velocity data over VCP 11
	12: Focuses on lower elevations to better sample the lower levels of storms.
	212: Improves range-obscured velocity data over VCP 12
	121: Scans lower cuts multiple times with varying pulse repetition to greatly
		 enhance velocity data
    21: Rarely used for convenction due to sparse elevation data and long completion time
	221: Improves range-obscured velocity data over VCP 121
	31: Long-pulse
	32: Short-pulse
*/
func (rh *RadarHeaderBlock) ScanDuration() time.Duration {
	switch rh.VolumeScanPattern {
	case 11, 211, 21, 221:
		return time.Minute * 5
	case 12, 212:
		return time.Minute*4 + time.Second*30
	case 121:
		return time.Minute * 6
	case 31, 32:
		return time.Minute * 10
	default:
		return time.Minute * 20
	}
}

func (rh *RadarHeaderBlock) IsDue() bool {
	return time.Now().UTC().Sub(rh.GetDate()) > rh.ScanDuration()+time.Minute*6
}

func (rh *RadarHeaderBlock) IsOld() bool {
	//Give 6 minutes for upload and twice the time
	//If it is this old, then there is something wrong
	if time.Now().UTC().Sub(rh.GetDate()) > (rh.ScanDuration()*2)+time.Minute*6 {
		return true
	}
	return false
}

type ProductDescriptionBlock struct {
	Divider              int16 //-1
	LatitudeOfRadar      int32 //Latitude*1000, + is north
	LongitudeOfRadar     int32 //Longitude*1000, + is east
	HeightAboveSea       int16 //In Feet
	ProductCode          int16 //Radar Product Number
	OperationalMode      int16 // 0=Maintenance 1=Clear air, 2=Precipitation/Severe Weather
	VolumeScanPattern    int16 //RDA volume coverage pattern
	SequenceNumber       int16 // Sequence number of the request that generated the product
	VolumeScanNumber     int16 //Counter, cycles 1 to 80
	VolumeScanDate       int16
	VolumeScanTime       int32
	ProductGenDate       int16
	ProductGenTime       int32
	P1                   int16 //Product Specific Code
	P2                   int16 //Product Specific Code
	ElevNum              int16 //Elevation scan within volume scan
	P3                   int16
	DataThreshold        [32]byte //Data Thresholds 16, 2 byte halfwords
	P4                   int16
	P5                   int16
	P6                   int16
	P7                   int16
	P8                   int16
	P9                   int16
	P10                  int16
	NumberOfMaps         int16
	OffsetSymbologyBlock int32 //Number of halfwords (2 bytes) from start of product to block
	OffsetGraphicBlock   int32
	OffsetTabularBlock   int32
}

func (pd *ProductDescriptionBlock) GetVolumeScanDate() time.Time {
	radarDay := time.Unix(0, 0).AddDate(0, 0, int(pd.VolumeScanDate))
	return radarDay.Add(time.Second * time.Duration(pd.VolumeScanTime))
}
func (pd *ProductDescriptionBlock) GetProductGenDate() time.Time {
	radarDay := time.Unix(0, 0).AddDate(0, 0, int(pd.ProductGenDate))
	return radarDay.Add(time.Second * time.Duration(pd.ProductGenTime))
}

func GetRadarHeader(data []byte) (*RadarHeaderBlock, error) {
	buf := bytes.NewBuffer(data[30:])
	rh := new(RadarHeaderBlock)
	err := binary.Read(buf, binary.BigEndian, rh)
	if err != nil {
		return nil, err
	}
	return rh, nil
}
