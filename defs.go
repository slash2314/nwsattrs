package nwsattrs

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	IMGWIDTH           = 600
	IMGHEIGHT          = 550
	MILESPERPIXHEIGHT  = IMGHEIGHT
	NMPERDEGLATEQUATOR = 60.10774
)

const (
	STORMCELLREG      = "[A-Z][0-9][ ]+[0-9]+/( *|[0-9])[0-9]+"
	MESOCELLREG       = "[0-9]+/[ ]*[0-9]+ +[0-9]?[0-9][L| ]"
	TVSCELLREG        = "[A-Z][0-9][ ]+[0-9]+/[ ]*[0-9]+"
	WARNINGFILEREGEXP = "warnings_[0-9]+_[0-9]+\\.txt"
)

//var Stations map[string][]interface{}
var Stations map[string]*StationLocation

func init() {
	var err error
	Stations, err = loadStations(os.Getenv("GOPATH") + "/src/bitbucket.org/slash2314/nwsattrs/StationLocations")
	if err != nil {
		log.Println(err)
	}
}

type StationLocation struct {
	LatLng
	LatLngBounds
}

type CellData struct {
	StormCellData *StormCellData
	MesoCellData  *MesoCellData
	TvsCellData   *TvsCellData
}

type StormCell struct {
	CellId, StationId, Az, Ran, Tvs, Meso, Posh, Poh, MaxSize,
	Vil, VilDensity, MaxDbz, Hgt, Top, FcstDir, FcstSpeed string
	PointLatLng LatLng
	LatLng
	ProductTime time.Time
}
type MesoCell struct {
	StationId, Strength, StormId, LowLevelRotationalVelocity, LowLevelDeltaVelocity,
	Base, Depth, Hgt, StrmRelPercent, MaxRotationalVelocity, Msi, FcstDir, FcstSpeed string
	PointLatLng LatLng
	LatLng
	ProductTime time.Time
}

type TvsCell struct {
	StationId, StormId, AverageDeltaVelocity, LowLevelDeltaVelocity, MaxDeltaVelocity, Hgt,
	Depth, Base, Top, MaxShear string
	LatLng
	ProductTime time.Time
}

type WarningCache struct{}

func (wc WarningCache) GetLatest() (interface{}, error) {
	return GetNWSWarnings(), nil
}

func loadStations(name string) (map[string]*StationLocation, error) {
	stations := make(map[string]*StationLocation)
	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	commaReader := csv.NewReader(f)
	locations, err := commaReader.ReadAll()
	if err != nil {
		return stations, err
	}
	for _, location := range locations {
		station := location[0]
		stationLat, _ := strconv.ParseFloat(location[1], 64)
		stationLon, _ := strconv.ParseFloat(location[2], 64)
		NEBoundLat, _ := strconv.ParseFloat(location[3], 64)
		NEBoundLon, _ := strconv.ParseFloat(location[4], 64)
		SWBoundLat, _ := strconv.ParseFloat(location[5], 64)
		SWBoundLon, _ := strconv.ParseFloat(location[6], 64)
		stations[station] = &StationLocation{
			LatLng: LatLng{stationLat, stationLon},
			LatLngBounds: LatLngBounds{
				LatLng{NEBoundLat, NEBoundLon},
				LatLng{SWBoundLat, SWBoundLon},
			},
		}
	}
	return stations, nil
}
