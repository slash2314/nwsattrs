package nwsattrs

import (
	"bytes"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	stormCellPattern     = regexp.MustCompile(STORMCELLREG)
	mesoCellStartPattern = regexp.MustCompile(MESOCELLREG)
	tvsPattern           = regexp.MustCompile(TVSCELLREG)
)

type IProductInfo interface {
	ScanDuration() time.Duration
	GetDate() time.Time
	IsDue() bool
	IsOld() bool
}

type StormCellData struct {
	Cells []StormCell
	IProductInfo
}

type MesoCellData struct {
	Cells []MesoCell
	IProductInfo
}
type TvsCellData struct {
	Cells []TvsCell
	IProductInfo
}

//TODO: Maybe include the radar status message? from http://weather.noaa.gov/pub/SL.us008001/DF.of/DC.radar/DS.75ftm/
func (rs *RadarStation) ParseStormCells() (*StormCellData, error) {
	rawData, err := getURLData(fmt.Sprintf("http://weather.noaa.gov/pub/SL.us008001/DF.of/DC.radar/DS.p37cr/SI.%s/sn.last", strings.ToLower(rs.Id)))
	if err != nil {
		return nil, err
	}
	radarHeader, err := GetRadarHeader(rawData)
	if err != nil {
		return nil, err
	}
	cellIndices := stormCellPattern.FindAllIndex(rawData, -1)
	stormCells := make([]StormCell, len(cellIndices))
	var cellData, cellId, az, ran, tvs, meso, posh, poh, maxSize,
		vil, maxDbz, hgt, top, fcstMvmt, fcstSpeed, fcstDir, topFormatted string
	var topValue float64
	var pointLatLng LatLng
	var vilValue int
	var fcstMvmtSplit []string
	for i, cellIndex := range cellIndices {
		cellData = string(rawData[cellIndex[0] : cellIndex[0]+66])
		cellData = strings.Replace(cellData, "   UNKNOWN   ", "0   0   0.00", -1)
		cellId = strings.TrimSpace(cellData[0:2])
		az = strings.TrimSpace(cellData[4:7])
		ran = strings.TrimSpace(cellData[8:12])
		tvs = strings.TrimSpace(cellData[12:16])
		meso = strings.TrimSpace(cellData[17:21])
		posh = strings.TrimSpace(cellData[23:26])
		poh = strings.TrimSpace(cellData[27:30])
		maxSize = strings.TrimSpace(cellData[31:36])
		vil = strings.TrimSpace(cellData[40:43])
		maxDbz = strings.TrimSpace(cellData[42:46])
		hgt = strings.TrimSpace(cellData[47:51])
		top = strings.TrimSpace(cellData[52:58])
		fcstMvmt = strings.TrimSpace(cellData[58:])
		if fcstMvmt == "NEW" {
			fcstSpeed = "N"
			fcstDir = "N"
		} else {
			fcstMvmtSplit = strings.SplitN(fcstMvmt, "/", 2)
			fcstDir = fcstMvmtSplit[0]
			fcstSpeed = strings.TrimSpace(fcstMvmtSplit[1])
		}
		topFormatted = strings.TrimSpace(top)
		if top[0] == '>' {
			topFormatted = strings.TrimSpace(topFormatted[1:])
		}
		topValue, err = strconv.ParseFloat(topFormatted, 64)
		vilValue, err = strconv.Atoi(vil)
		if err != nil {
			log.Println("Something happened with the top conversion", topFormatted)
			continue
		}
		vilDensity := fmt.Sprintf("%.2f", (float64(vilValue)/(topValue*304.8))*1000)
		stormLatLng := rs.getLatLon(az, ran)
		if fcstDir != "N" {
			pointLatLng = hourMovementPoint(fcstSpeed, fcstDir, stormLatLng)
		}
		stormCells[i] = StormCell{
			CellId:      cellId,
			StationId:   rs.Id,
			Az:          az,
			Ran:         ran,
			Tvs:         tvs,
			Meso:        meso,
			Posh:        posh,
			Poh:         poh,
			MaxSize:     maxSize,
			Vil:         vil,
			VilDensity:  vilDensity,
			MaxDbz:      maxDbz,
			Hgt:         hgt,
			Top:         topFormatted,
			FcstDir:     fcstDir,
			FcstSpeed:   fcstSpeed,
			PointLatLng: pointLatLng,
			LatLng:      stormLatLng,
			ProductTime: radarHeader.GetDate(),
		}
	}
	stormCellInfo := &StormCellData{
		Cells:        stormCells,
		IProductInfo: radarHeader,
	}
	return stormCellInfo, nil
}

func (rs *RadarStation) ParseMesoCells() (*MesoCellData, error) {
	rawData, err := getURLData(fmt.Sprintf("http://weather.noaa.gov/pub/SL.us008001/DF.of/DC.radar/DS.141md/SI.%s/sn.last", strings.ToLower(rs.Id)))
	if err != nil {
		return nil, err
	}

	rawMesoString := rawData
	mh, err := GetRadarHeader(rawData)
	if err != nil {
		return nil, err
	}
	begin := bytes.Index(rawMesoString, []byte("deg/kts")) + 80
	cellIndices := mesoCellStartPattern.FindAllIndex(rawData[begin:], -1)
	cleanMesoString := string(rawMesoString[begin:])

	mesoCells := make([]MesoCell, len(cellIndices))
	var mesoCellInfo, az, ran, strength, stormID, llrv, lldv, base, depth, strmRelPercent,
		hgt, maxRV, msi, fcstDir, fcstSpeed string
	var pointLatLng LatLng
	for i, cellIndex := range cellIndices {
		mesoCellInfo = cleanMesoString[cellIndex[0] : cellIndex[0]+72]
		az = strings.TrimSpace(mesoCellInfo[0:3])
		ran = strings.TrimSpace(mesoCellInfo[4:7])
		mesoLatLng := rs.getLatLon(az, ran)
		strength = strings.TrimSpace(mesoCellInfo[8:11])
		stormID = mesoCellInfo[12:14]
		llrv = strings.TrimSpace(mesoCellInfo[15:18])
		lldv = strings.TrimSpace(mesoCellInfo[19:24])
		base = strings.TrimSpace(mesoCellInfo[25:28])
		depth = strings.TrimSpace(mesoCellInfo[31:35])
		strmRelPercent = strings.TrimSpace(mesoCellInfo[36:39])
		hgt = strings.TrimSpace(mesoCellInfo[45:47])
		maxRV = strings.TrimSpace(mesoCellInfo[52:54])
		msi = strings.TrimSpace(mesoCellInfo[68:])
		fcstMvmt := strings.TrimSpace(mesoCellInfo[59:67])
		if fcstMvmt == "" {
			fcstDir = "N"
			fcstSpeed = "N"
		} else {
			fcstMvmtSplit := strings.SplitN(fcstMvmt, "/", 2)
			fcstDir = fcstMvmtSplit[0]
			fcstSpeed = strings.TrimSpace(fcstMvmtSplit[1])
			if fcstSpeed == "0" {
				fcstSpeed = "N"
				fcstDir = "N"
			}
		}
		if fcstDir != "N" {
			pointLatLng = hourMovementPoint(fcstSpeed, fcstDir, mesoLatLng)
		}
		mesoCells[i] = MesoCell{
			StationId:                  rs.Id,
			Strength:                   strength,
			StormId:                    stormID,
			LowLevelRotationalVelocity: llrv,
			LowLevelDeltaVelocity:      lldv,
			Base:                  base,
			Depth:                 depth,
			Hgt:                   hgt,
			StrmRelPercent:        strmRelPercent,
			MaxRotationalVelocity: maxRV,
			Msi:         msi,
			FcstDir:     fcstDir,
			FcstSpeed:   fcstSpeed,
			PointLatLng: pointLatLng,
			LatLng:      mesoLatLng,
			ProductTime: mh.GetDate(),
		}
	}
	mesoCellData := &MesoCellData{
		Cells:        mesoCells,
		IProductInfo: mh,
	}
	return mesoCellData, nil

}

func (rs *RadarStation) ParseTvsCells() (*TvsCellData, error) {
	rawData, err := getHtml(fmt.Sprintf("http://weather.noaa.gov/pub/SL.us008001/DF.of/DC.radar/DS.61tvs/SI.%s/sn.last", strings.ToLower(rs.Id)))
	if err != nil {
		return nil, err
	}
	tvsHead, err := GetRadarHeader([]byte(rawData))
	if err != nil {
		return nil, err
	}

	tvsIndicies := tvsPattern.FindAllIndex([]byte(rawData), -1)
	tvsString := string(rawData)
	tvsCells := make([]TvsCell, len(tvsIndicies))
	var currentTvs, stormID, az, ran, avgdv, lldv, mxdv, hgt, depth, base, top,
		mxShear string
	for i, match := range tvsIndicies {
		currentTvs = tvsString[match[0] : match[0]+67]
		stormID = strings.TrimSpace(currentTvs[0:2])
		az = strings.TrimSpace(currentTvs[5:8])
		ran = strings.TrimSpace(currentTvs[9:12])
		tvsLatLng := rs.getLatLon(az, ran)
		avgdv = strings.TrimSpace(currentTvs[15:18])
		lldv = strings.TrimSpace(currentTvs[21:24])
		mxdv = strings.TrimSpace(currentTvs[27:30])
		hgt = strings.TrimSpace(currentTvs[31:35])
		depth = strings.TrimSpace(currentTvs[38:43])
		base = strings.TrimSpace(currentTvs[45:50])
		top = strings.TrimSpace(currentTvs[52:56])
		mxShear = strings.TrimSpace(currentTvs[60:62])
		tvsCells[i] = TvsCell{
			StationId:             rs.Id,
			StormId:               stormID,
			AverageDeltaVelocity:  avgdv,
			LowLevelDeltaVelocity: lldv,
			MaxDeltaVelocity:      mxdv,
			Hgt:                   hgt,
			Depth:                 depth,
			Base:                  base,
			Top:                   top,
			MaxShear:              mxShear,
			LatLng:                tvsLatLng,
			ProductTime:           tvsHead.GetDate(),
		}
	}
	tvsCellData := &TvsCellData{
		Cells:        tvsCells,
		IProductInfo: tvsHead,
	}
	return tvsCellData, nil
}
