package nwsattrs

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var urlClient = http.Client{
	Timeout: time.Second * 5,
}

func getURLData(url string) ([]byte, error) {
	r, err := urlClient.Get(url)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer r.Body.Close()
	return ioutil.ReadAll(r.Body)
}

func getHtml(url string) (htmlString string, err error) {
	r, err := urlClient.Get(url)
	if err != nil {
		log.Println(err)
		return
	}
	defer r.Body.Close()
	rawData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		return
	}
	return string(rawData), nil
}
