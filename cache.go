package nwsattrs

import (
	"reflect"
	"sync"
	"time"
)

type Cacher interface {
	GetLatest() (interface{}, error)
}

type Cache struct {
	Cacher
	cacheInterval time.Duration
	timeToLive    time.Duration
	cacheData     interface{}
	override      bool
	lastUpdated   time.Time
	*sync.Mutex
}

func NewCache(c Cacher, cacheDuration time.Duration, timeToLive time.Duration) *Cache {
	return &Cache{
		Cacher:        c,
		cacheInterval: cacheDuration,
		timeToLive:    timeToLive,
		override:      false,
		Mutex:         new(sync.Mutex),
	}
}

func (c *Cache) GetCachedData() (interface{}, bool, error) {
	c.Lock()
	defer c.Unlock()
	if time.Since(c.lastUpdated) < c.cacheInterval && !c.override {
		return c.cacheData, false, nil
	}
	latestData, err := c.GetLatest()
	if err != nil {
		if time.Since(c.lastUpdated) < c.timeToLive && c.cacheData != nil {
			return c.cacheData, false, err
		} else {
			return nil, false, err
		}
	}

	c.lastUpdated = time.Now()
	if reflect.DeepEqual(latestData, c.cacheData) {
		return c.cacheData, false, nil
	}
	c.cacheData = latestData
	c.override = false
	return c.cacheData, true, nil
}

func (c *Cache) Reset() {
	c.Lock()
	defer c.Unlock()
	c.override = true
}
