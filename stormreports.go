package nwsattrs

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	TORNREPORTURL = "http://www.spc.noaa.gov/climo/reports/today_torn.csv"
	WINDREPORTURL = "http://www.spc.noaa.gov/climo/reports/today_wind.csv"
	HAILREPORTURL = "http://www.spc.noaa.gov/climo/reports/today_hail.csv"
)

type Report struct {
	Time     time.Time
	Location string
	County   string
	State    string
	*LatLng
	Comments string
	SpecAttr string `json:"-"`
}
type WindReport struct {
	Report
	Speed string
}
type HailReport struct {
	Report
	Size string
}
type TornadoReport struct {
	Report
	FScale string
}

func parseFields(r io.Reader) (records [][]string, err error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	entries := bytes.Split(data, []byte("\n"))[1:]
	for _, entry := range entries {
		fields := strings.Split(strings.Trim(string(entry), "\n\r"), ",")
		if len(fields) > 8 {
			//Sometimes the comment section contains a comma. They
			//should have made it a quote fild, but they didn't.
			fields[7] = strings.Join(fields[7:], ", ")
			fields = fields[:8]
		} else if len(fields) < 8 {
			continue
		}
		records = append(records, fields)
	}
	return records, nil
}

// A client that will timeout after 5 seconds

func getFields(url string) (records [][]string, err error) {
	var reportHttpClient = http.Client{
		Timeout: time.Second * 5,
	}
	r, err := reportHttpClient.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Problem in getfields: %v\n", err)
	}
	defer r.Body.Close()
	fields, err := parseFields(r.Body)
	return fields, err
}

func formatTime(rawTime string) (time.Time, error) {
	t, err := time.Parse("2006-01-02 15:04", time.Now().UTC().Format("2006-01-02")+" "+rawTime[:2]+":"+rawTime[2:])
	if t.After(time.Now().UTC()) {
		t = t.AddDate(0, 0, -1)
	}
	return t, err
}

func getLatLng(lat, lng string) (*LatLng, error) {
	floatLat, err := strconv.ParseFloat(lat, 64)
	if err != nil {
		return nil, err
	}
	floatLng, err := strconv.ParseFloat(lng, 64)
	if err != nil {
		return nil, err
	}
	return &LatLng{floatLat, floatLng}, nil
}

func GetReports(reportUrl string) (reports []Report, err error) {
	fields, err := getFields(reportUrl)
	if err != nil {
		return nil, err
	}
	reports = make([]Report, len(fields))
	for _, field := range fields {
		reportTime, err := formatTime(field[0])
		if err != nil {
			return nil, err
		}
		latLng, err := getLatLng(field[5], field[6])
		if err != nil {
			return nil, err
		}
		reports = append(reports, Report{Time: reportTime, Location: field[2], County: field[3], State: field[4], LatLng: latLng, Comments: field[7], SpecAttr: field[1]})
	}
	return reports, nil
}

func GetWindReports() (windReports []WindReport, err error) {
	reports, err := GetReports(WINDREPORTURL)
	if err != nil {
		return nil, err
	}
	windReports = []WindReport{}
	for _, r := range reports {
		if time.Now().UTC().Sub(r.Time) <= time.Hour*1 {
			wr := WindReport{
				Report: r,
				Speed:  r.SpecAttr,
			}
			windReports = append(windReports, wr)
		}
	}
	return windReports, nil
}

func GetHailReports() (hailReports []HailReport, err error) {
	reports, err := GetReports(HAILREPORTURL)
	if err != nil {
		return nil, err
	}
	hailReports = []HailReport{}
	for _, r := range reports {
		if time.Now().UTC().Sub(r.Time) <= time.Hour*1 {
			hailSize := fmt.Sprintf("%s.%s", r.SpecAttr[0:1], r.SpecAttr[1:])
			hailReports = append(hailReports, HailReport{Report: r, Size: hailSize})
		}
	}
	return hailReports, nil
}

func GetTornadoReports() (tornadoReports []TornadoReport, err error) {
	reports, err := GetReports(TORNREPORTURL)
	if err != nil {
		return nil, err
	}
	tornadoReports = []TornadoReport{}
	for _, r := range reports {
		if time.Now().UTC().Sub(r.Time) <= time.Hour*1 {
			tornadoReports = append(tornadoReports, TornadoReport{Report: r, FScale: r.SpecAttr})
		}
	}
	return tornadoReports, nil
}

type TornadoReportsCacher struct{}
type WindReportsCacher struct{}
type HailReportsCacher struct{}

func (rt TornadoReportsCacher) GetLatest() (interface{}, error) {
	return GetTornadoReports()
}
func (rt WindReportsCacher) GetLatest() (interface{}, error) {
	return GetWindReports()
}
func (rt HailReportsCacher) GetLatest() (interface{}, error) {
	return GetHailReports()
}
