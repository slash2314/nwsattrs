package nwsattrs

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

var warningFilePattern = regexp.MustCompile(WARNINGFILEREGEXP)

type WarningStatus string

const (
	NEW      WarningStatus = "O.NEW"
	EXPIRED                = "O.EXP"
	CONTINUE               = "O.CON"
	CANCEL                 = "O.CAN"
)

type Warning struct {
	ExpireTime                            string
	WarningName, WarningType, Description string
	Polygon                               []LatLng
}

func (w Warning) GetPolygon() []LatLng { return w.Polygon }

type SortByWarningName []Warning

func (w SortByWarningName) Len() int {
	return len(w)
}

func (w SortByWarningName) Less(i, j int) bool { return w[i].WarningName < w[j].WarningName }

func (w SortByWarningName) Swap(i, j int) { w[i], w[j] = w[j], w[i] }

func GetWarningStatus(text string) WarningStatus {
	switch {
	case strings.Contains(text, string(NEW)):
		return NEW
	case strings.Contains(text, string(EXPIRED)):
		return EXPIRED
	case strings.Contains(text, string(CONTINUE)):
		return CONTINUE
	default:
		return CANCEL
	}

}

func GetWarningTexts(url string, number int) (warningTexts []string, err error) {
	warningData, err := getHtml(url)
	if err != nil {
		return warningTexts, err
	}
	warningFiles := warningFilePattern.FindAllString(warningData, -1)
	if len(warningFiles) >= number {
		warningFiles = warningFiles[len(warningFiles)-number:]
	}
	for _, warningFile := range warningFiles {
		warningText, err := getHtml(url + "/" + warningFile)
		if err != nil {
			return warningTexts, err
		}
		warningsSplit := strings.Split(warningText, "$$")
		warningTexts = append(warningTexts, warningsSplit...)
	}
	return warningTexts, nil
}

func GetNWSWarnings() []Warning {
	warningMap := make(map[string]Warning)
	var expireTime time.Time
	var description, latlng, warningName, warningType string
	var timeStringIndex, coordStart, findEndLatLng int
	var latlngSplit []string
	warningTexts, err := GetWarningTexts("http://warnings.cod.edu", 4)
	if err != nil {
		fmt.Fprintln(os.Stderr, "There was an error retrieving warning texts.")
		return nil
	}
	for _, warning := range warningTexts {
		var polyCoords []LatLng
		timeStringIndex = strings.Index(warning, "/O.") + 1
		if timeStringIndex == 0 {
			continue
		}
		warningName = warning[timeStringIndex+6 : timeStringIndex+20]
		warningType = strings.Replace(warning[timeStringIndex+11:timeStringIndex+15], ".", "", 1)
		expireTime, err = time.Parse("060102T1504", warning[timeStringIndex+34:timeStringIndex+45])
		if err != nil {
			log.Println("There was an issue with the warning time")
			continue
		}
		//Won't add the warning if it is already past expiration
		if time.Now().UTC().After(expireTime) {
			continue
		}
		coordStart = strings.Index(warning, "LAT...LON")
		if coordStart == -1 || timeStringIndex == -1 {
			log.Println("Error: Could not find the start of Latitude/Longitude pairs")
			continue
		}
		if coordStart < timeStringIndex+48 {
			log.Println("Could not find the start of the Latitude/Longitude pairs")
			continue
		}
		if warningType == "FFW" {
			description = warning[timeStringIndex+105 : coordStart]
		} else {
			description = warning[timeStringIndex+48 : coordStart]
		}
		description = strings.Replace(description, "\n", "<br>", -1)
		description = strings.Replace(description, "&&", "", -1)
		latlng = warning[coordStart:]
		if err != nil || len(latlng) < 10 {
			continue
		}
		warningStatus := GetWarningStatus(warning)
		if warningStatus == NEW || warningStatus == CONTINUE {
			if warningType == "FFW" {
				latlngSplit = strings.Split(strings.Replace(latlng[10:], "\r\r\n      ", " ", -1), " ")
			} else {
				findEndLatLng = strings.Index(latlng, "TIME")
				if findEndLatLng == -1 {
					continue
				}
				latlngSplit = strings.Split(strings.Replace(latlng[10:findEndLatLng], "\r\r\n      ", " ", -1), " ")
			}
			for i := 0; i < len(latlngSplit)-1; i += 2 {
				rawLat := strings.TrimSpace(latlngSplit[i])
				rawLng := strings.TrimSpace(latlngSplit[i+1])
				if len(rawLat) != 4 {
					fmt.Fprintln(os.Stderr, "Something is wrong with the Lat format")
					continue
				}
				lat, err := strconv.ParseFloat(rawLat, 64)
				if err != nil {
					fmt.Fprintln(os.Stderr, "Something is wrong with the Lat format")
					continue
				}
				lng, err := strconv.ParseFloat(rawLng, 64)
				if err != nil {
					fmt.Fprintln(os.Stderr, "Something is wrong with the Lng format")
					continue
				}
				polyCoords = append(polyCoords, LatLng{lat * 0.01, lng * -0.01})
			}
			warningMap[warningName] = Warning{
				ExpireTime:  expireTime.Format(time.RFC1123Z),
				WarningName: warningName,
				WarningType: warningType,
				Description: description,
				Polygon:     polyCoords,
			}

		} else if _, ok := warningMap[warningName]; ok {
			//If it is canceled, delete it from the warning map
			delete(warningMap, warningName)
		}
	}
	warnings := []Warning{}
	for _, warning := range warningMap {
		warnings = append(warnings, warning)
	}
	//Make sure that the array is consistantly in order.
	sort.Sort(SortByWarningName(warnings))
	return warnings
}
